package hr.fer.tel.spark.ml.gwr.cluster

import hr.fer.tel.spark.ml.gwr.cluster
import org.apache.spark.ml.clustering.{GaussianMixture, GaussianMixtureModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

class GeospatialGaussianMixture(val templateModel: GaussianMixture)(implicit spark: SparkSession)
  extends BaseGeospatialClustering {

  override protected def fitInternal(df: DataFrame,
                                     centroidCol: String,
                                     clusterCol: String): GeospatialClusteringModel = {
    val tempModel = templateModel.copy(ParamMap.empty)
    tempModel.setFeaturesCol(centroidCol)
    tempModel.setPredictionCol(clusterCol)

    new GeospatialGaussianMixtureModel(tempModel.fit(df))
  }
}

object GeospatialGaussianMixtureModel extends GeospatialClusteringModelLoader {

  override def load(path: String)(implicit spark: SparkSession): GeospatialGaussianMixtureModel = {
    new cluster.GeospatialGaussianMixtureModel(GaussianMixtureModel.load(path))
  }
}

class GeospatialGaussianMixtureModel(private val _model: GaussianMixtureModel)(implicit spark: SparkSession)
  extends BaseGeospatialClusteringModel {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df)

  override def k: Int = _model.getK

  def underlyingModel: GaussianMixtureModel = _model

  override def save(path: String): Unit = _model.save(path)
}

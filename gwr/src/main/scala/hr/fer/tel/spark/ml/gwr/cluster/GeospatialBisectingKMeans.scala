package hr.fer.tel.spark.ml.gwr.cluster

import hr.fer.tel.spark.ml.gwr.cluster
import org.apache.spark.ml.clustering.{BisectingKMeans, BisectingKMeansModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

class GeospatialBisectingKMeans(val templateModel: BisectingKMeans)(implicit spark: SparkSession)
  extends BaseGeospatialClustering {

  override protected def fitInternal(df: DataFrame,
                                     centroidCol: String,
                                     clusterCol: String): GeospatialClusteringModel = {
    val tempModel = templateModel.copy(ParamMap.empty)
    tempModel.setFeaturesCol(centroidCol)
    tempModel.setPredictionCol(clusterCol)

    new GeospatialBisectingKMeansModel(tempModel.fit(df))
  }
}

object GeospatialBisectingKMeansModel extends GeospatialClusteringModelLoader {

  override def load(path: String)(implicit spark: SparkSession): GeospatialBisectingKMeansModel = {
    new cluster.GeospatialBisectingKMeansModel(BisectingKMeansModel.load(path))
  }
}

class GeospatialBisectingKMeansModel(private val _model: BisectingKMeansModel)(implicit spark: SparkSession)
  extends BaseGeospatialClusteringModel {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df)

  override def k: Int = _model.getK

  def underlyingModel: BisectingKMeansModel = _model

  override def save(path: String): Unit = _model.save(path)
}

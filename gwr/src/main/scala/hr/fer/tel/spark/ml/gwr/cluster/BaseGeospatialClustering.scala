package hr.fer.tel.spark.ml.gwr.cluster

import org.apache.spark.sql.{DataFrame, SparkSession}

abstract class BaseGeospatialClustering(implicit spark: SparkSession)
  extends GeospatialClustering with Serializable {

  protected def fitInternal(df: DataFrame, centroidCol: String, clusterCol: String): GeospatialClusteringModel

  override def fit(df: DataFrame,
                   prepared: Boolean = false,
                   geometryCol: String = "geometry",
                   centroidCol: String = "centroid",
                   clusterCol: String = "cluster"): GeospatialClusteringModel = {
    fitInternal(
      if (prepared) df else CentroidFinder.find(df, geometryCol, centroidCol),
      centroidCol,
      clusterCol
    )
  }
}


abstract class BaseGeospatialClusteringModel(implicit spark: SparkSession)
  extends GeospatialClusteringModel with Serializable {

  protected def transformInternal(df: DataFrame): DataFrame

  override def transform(df: DataFrame,
                         prepared: Boolean = false,
                         geometryCol: String = "geometry",
                         centroidCol: String = "centroid"): DataFrame = {
    transformInternal(
      if (prepared) df else CentroidFinder.find(df, geometryCol, centroidCol)
    )
  }
}
package hr.fer.tel.spark.ml.gwr.kernel

class BoxCarKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double = if (math.abs(value) < bandwidth) 1 else 0
}

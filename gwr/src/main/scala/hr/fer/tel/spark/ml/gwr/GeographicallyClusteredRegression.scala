package hr.fer.tel.spark.ml.gwr

import hr.fer.tel.spark.ml.gwr.cluster._
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.param.{Param, ParamMap, Params}
import org.apache.spark.ml.regression.{GeneralizedLinearRegression, GeneralizedLinearRegressionModel}
import org.apache.spark.ml.util._
import org.apache.spark.ml.{Estimator, Model}
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.{DoubleType, StructField, StructType}
import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}

import java.io.PrintWriter
import java.util.Objects.nonNull
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source


object GeographicallyClusteredRegression {
  private[gwr] val _clusterCol = "cluster"
}

trait GeographicallyClusteredRegressionParams extends Params with HasGeometryCol with HasGeneralizedLinearRegression {
  def geospatialClustering: Param[GeospatialClustering] = new Param[GeospatialClustering](
    this, "geospatialClustering", "GeospatialClustering implementation used for grouping geospatial data.")

  def getGeospatialClustering: GeospatialClustering = $(geospatialClustering)
}

class GeographicallyClusteredRegression(override val uid: String)(implicit spark: SparkSession)
  extends Estimator[GeographicallyClusteredRegressionModel]
    with Serializable with GeographicallyClusteredRegressionParams {

  import GeographicallyClusteredRegression._

  private val _centroidCol = "centroid"

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("gcr"))(spark)

  def setGeometryCol(value: String): GeographicallyClusteredRegression = set(geometryCol, value)

  def setGeospatialClustering(value: GeospatialClustering): GeographicallyClusteredRegression = set(geospatialClustering, value)

  setDefault(geospatialClustering, new GeospatialKMeans(new KMeans()))

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeographicallyClusteredRegression = set(generalizedLinearRegression, value)

  override def fit(dataset: Dataset[_]): GeographicallyClusteredRegressionModel = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    var df = dataset.toDF()
    import spark.implicits._

    val clusteringModel = getGeospatialClustering.fit(df,
      centroidCol = _centroidCol,
      geometryCol = getGeometryCol,
      clusterCol = _clusterCol
    )

    df = clusteringModel.transform(df,
      centroidCol = _centroidCol,
      geometryCol = getGeometryCol
    )

    val glrModels = (0 until clusteringModel.k) map { clusterId =>
      Future {
        val trainDf = df.where($"${_clusterCol}" === clusterId)
        if (trainDf.count() == 0L) {
          null
        } else {
          (clusterId, getGeneralizedLinearRegression.fit(trainDf))
        }
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, Duration.Inf)
    } map {
      job => Await.result(job, Duration.Inf)
    } filter nonNull toMap

    new GeographicallyClusteredRegressionModel(clusteringModel, glrModels)
      .setGeometryCol(getGeometryCol)
  }

  override def copy(extra: ParamMap): GeographicallyClusteredRegression = {
    new GeographicallyClusteredRegression(uid)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression.copy(extra)))
  }

  override def transformSchema(schema: StructType): StructType = schema
}

object GeographicallyClusteredRegressionModel extends MLReadable[GeographicallyClusteredRegressionModel] {

  private var _clusteringModelLoader: GeospatialClusteringModelLoader = GeospatialKMeansModel

  def setClusteringModelLoader(clusteringModelLoader: GeospatialClusteringModelLoader): GeographicallyClusteredRegressionModel.type = {
    _clusteringModelLoader = clusteringModelLoader
    this
  }

  override def read: MLReader[GeographicallyClusteredRegressionModel] = {
    new GeographicallyClusteredRegressionModelReader(_clusteringModelLoader)
  }

  private[gwr] class GeographicallyClusteredRegressionModelReader(clusteringModelLoader: GeospatialClusteringModelLoader)
    extends MLReader[GeographicallyClusteredRegressionModel] {

    override def load(path: String): GeographicallyClusteredRegressionModel = {
      implicit val spark: SparkSession = this.sparkSession

      val clusteringModel = clusteringModelLoader.load(s"$path/_clusteringModel")
      val glrModels = (0 until clusteringModel.k)
        .map {
          clusterId =>
            (clusterId, try {
              GeneralizedLinearRegressionModel.load(s"$path/_$clusterId")
            } catch {
              case _: Throwable => null
            })
        } filter { t => t._2 != null } toMap

      val columnsSource = Source.fromFile(s"$path/params")
      val columns = columnsSource.mkString.split(",")
      columnsSource.close()

      new GeographicallyClusteredRegressionModel(clusteringModel, glrModels)
        .setGeometryCol(columns(0))
    }
  }

}

class GeographicallyClusteredRegressionModel private[gwr](override val uid: String,
                                                          private val _clusteringModel: GeospatialClusteringModel,
                                                          private val _glrModels: Map[Int, GeneralizedLinearRegressionModel])
                                                         (implicit spark: SparkSession)
  extends Model[GeographicallyClusteredRegressionModel] with HasGeometryCol
    with MLWritable {

  import GeographicallyClusteredRegression._

  private[gwr] def this(_clusteringModel: GeospatialClusteringModel,
                        _glrModels: Map[Int, GeneralizedLinearRegressionModel])(implicit spark: SparkSession) =
    this(Identifiable.randomUID("gcrModel"), _clusteringModel, _glrModels)(spark)

  def setGeometryCol(value: String): GeographicallyClusteredRegressionModel = set(geometryCol, value)

  override def transform(dataset: Dataset[_]): DataFrame = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    val df = dataset.toDF()
    import org.apache.spark.sql.functions.col
    import spark.implicits._

    val selectColumns: Seq[Column] = df.columns.toList.map(col) ++ Seq(col(_glrModels.head._2.getPredictionCol))
    val tempDf = _clusteringModel.transform(df, geometryCol = getGeometryCol)

    (0 until _clusteringModel.k) map { clusterId =>
      Future {
        val testDf = tempDf.where($"${_clusterCol}" === clusterId)

        if (testDf.count() == 0) {
          testDf.withColumn(_glrModels.head._2.getPredictionCol, lit(null).cast(DoubleType))
        } else {
          _glrModels.get(clusterId) match {
            case None => testDf.withColumn(_glrModels.head._2.getPredictionCol, lit(null).cast(DoubleType))
            case Some(glr) => glr.transform(testDf)
          }
        }
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, Duration.Inf)
    } map {
      job => Await.result(job, Duration.Inf)
    } reduce {
      (df1, df2) => df1.union(df2)
    } select (selectColumns: _*)
  }

  override def copy(extra: ParamMap): GeographicallyClusteredRegressionModel = {
    new GeographicallyClusteredRegressionModel(uid, _clusteringModel, _glrModels)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
  }

  override def transformSchema(schema: StructType): StructType =
    schema.add(StructField(_glrModels.head._2.getPredictionCol, DoubleType))

  override def write: MLWriter = new GeographicallyClusteredRegressionModelWriter()

  private[gwr] class GeographicallyClusteredRegressionModelWriter extends MLWriter {
    override def saveImpl(path: String): Unit = {
      _clusteringModel.save(s"$path/_clusteringModel")
      _glrModels.zipWithIndex.foreach { case (glr, i) => glr._2.save(s"$path/_$i") }
      new PrintWriter(s"$path/params") {
        write(s"$getGeometryCol")
        close()
      }
    }
  }
}

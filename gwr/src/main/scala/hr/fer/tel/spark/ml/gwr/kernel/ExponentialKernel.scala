package hr.fer.tel.spark.ml.gwr.kernel

class ExponentialKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double = if (bandwidth == 0) 1 else math.exp(-0.5 * math.abs(value / bandwidth))
}

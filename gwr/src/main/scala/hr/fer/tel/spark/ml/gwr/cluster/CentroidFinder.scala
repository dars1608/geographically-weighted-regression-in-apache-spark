package hr.fer.tel.spark.ml.gwr.cluster

import com.vividsolutions.jts.geom.Geometry
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}

object CentroidFinder {
  private val centroidXCol = "centroid_x"
  private val centroidYCol = "centroid_y"

  private val stXUdf = "ST_X"
  private val stYUdf = "ST_Y"

  def find(df: DataFrame, geometryCol: String, centroidCol: String)(implicit spark: SparkSession): DataFrame = {
    import org.apache.spark.sql.functions.expr

    spark.udf.register(stXUdf, (geometry: Geometry) => geometry.getCentroid.getCoordinate.x)
    spark.udf.register(stYUdf, (geometry: Geometry) => geometry.getCentroid.getCoordinate.y)

    val tempDf = df
      .withColumn(centroidXCol, expr(f"$stXUdf($geometryCol)"))
      .withColumn(centroidYCol, expr(f"$stYUdf($geometryCol)"))

    new VectorAssembler()
      .setInputCols(Array(centroidXCol, centroidYCol))
      .setOutputCol(centroidCol)
      .transform(tempDf)
  }
}

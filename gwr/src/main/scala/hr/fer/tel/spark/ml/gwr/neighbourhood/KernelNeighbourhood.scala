package hr.fer.tel.spark.ml.gwr.neighbourhood

import org.apache.spark.sql.DataFrame

trait KernelNeighbourhood extends Serializable {
  def find(df: DataFrame,
           geometryCol: String = "geometry",
           featuresCol: String = "features",
           labelCol: String = "label",
           weightCol: String = "weight",
           rowNumberCol: String = "rn",
           kernelName: String = "kernel",
           trainView: String = "traindf",
           unknownView: String = "unknowndf"): DataFrame
}

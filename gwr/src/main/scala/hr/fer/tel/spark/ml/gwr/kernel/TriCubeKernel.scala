package hr.fer.tel.spark.ml.gwr.kernel

class TriCubeKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double = if (math.abs(value) < bandwidth) math.pow(1 - math.pow(value / bandwidth, 3), 3) else 0
}

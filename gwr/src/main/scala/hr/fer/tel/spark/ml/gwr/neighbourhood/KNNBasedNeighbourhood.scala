package hr.fer.tel.spark.ml.gwr.neighbourhood

import org.apache.spark.sql.DataFrame

class KNNBasedNeighbourhood(val bandwidthNeighbours: Int = 10) extends KernelNeighbourhood {

  val tmpView: String = "tmpresultdf"

  override def find(df: DataFrame,
                    geometryCol: String = "geometry",
                    featuresCol: String = "features",
                    labelCol: String = "label",
                    weightCol: String = "weight",
                    rowNumberCol: String = "rn",
                    kernelName: String = "kernel",
                    trainView: String = "traindf",
                    unknownView: String = "unknowndf"): DataFrame = {

    val selectUnknownLabel = if (df.columns.contains(labelCol)) {
      (s"$unknownView.$labelCol AS ${unknownView}_$labelCol,", s"${unknownView}_$labelCol,")
    } else {
      ("", "")
    }

    var result = df.sqlContext.sql(
      s"""
         | SELECT ${unknownView}_$rowNumberCol,
         |        ${unknownView}_$geometryCol,
         |        ${unknownView}_$featuresCol,
         |        ${selectUnknownLabel._2}
         |        ${trainView}_$featuresCol,
         |        ${trainView}_$labelCol,
         |        distance
         | FROM (SELECT $unknownView.$rowNumberCol AS ${unknownView}_$rowNumberCol,
         |        $unknownView.$geometryCol AS ${unknownView}_$geometryCol,
         |        $unknownView.$featuresCol AS ${unknownView}_$featuresCol,
         |        ${selectUnknownLabel._1}
         |        $trainView.$featuresCol AS ${trainView}_$featuresCol,
         |        $trainView.$labelCol AS ${trainView}_$labelCol,
         |         ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol) as distance,
         |         ROW_NUMBER() OVER(PARTITION BY $unknownView.$rowNumberCol
         |                           ORDER BY ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol)) as neighbour_number
         | FROM $unknownView CROSS JOIN $trainView)
         | WHERE neighbour_number <= $bandwidthNeighbours
         |""".stripMargin
    )

    result.createOrReplaceTempView(tmpView)
    result = df.sqlContext.sql(
      s"""
         | SELECT ${unknownView}_$rowNumberCol,
         |        ${unknownView}_$featuresCol,
         |        ${unknownView}_$geometryCol,
         |        ${selectUnknownLabel._2}
         |        ${trainView}_$featuresCol,
         |        ${trainView}_$labelCol,
         |        $kernelName($tmpView.distance, max($tmpView.distance) OVER(PARTITION BY $tmpView.${unknownView}_$rowNumberCol)) as $weightCol
         | FROM $tmpView
         |""".stripMargin
    )

    result
  }
}
